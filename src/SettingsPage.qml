import Flickr 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
    id: root

    property var model: null

    header: PageHeader {
        title: i18n.tr("Upload settings")
        flickable: flick
        leadingActionBar.actions: [
            Action {
                id: goBack
                iconName: "back"
                onTriggered: {
                    updateModel()
                    pageStack.pop()
                }
            }
        ]
    }
    Flickable {
        id: flick
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height

        Column {
            anchors { left: parent.left; right: parent.right; margins: units.gu(1) }

            ListItem.Caption {
                text: i18n.tr("Photos will be visible to:")
            }

            ListItem.Standard {
                text: i18n.tr("Only you (private)")
                control: CheckBox {
                    id: wPrivate
                    checked: !model.isPublic
                }
            }

            Column {
                anchors { left: parent.left; right: parent.right; leftMargin: units.gu(2) }
                ListItem.Standard {
                    enabled: wPrivate.checked
                    text: i18n.tr("Friends")
                    control: CheckBox {
                        id: wFriends
                        checked: model.isFriends
                    }
                }

                ListItem.Standard {
                    enabled: wPrivate.checked
                    text: i18n.tr("Family")
                    control: CheckBox {
                        id: wFamily
                        checked: model.isFamily
                    }
                }
            }

            ListItem.Standard {
                text: i18n.tr("Hide from searches")
                control: CheckBox {
                    id: wHidden
                    checked: model.isHidden
                }
            }

            ListItem.SingleControl {
                height: units.gu(6)
                control: Button {
                    text: i18n.tr("Remember as default")
                    anchors {
                        centerIn: parent
                        margins: units.gu(1)
                    }
                    onClicked: {
                        Settings.isPublic = !wPrivate.checked
                        Settings.isFriends = wFriends.checked
                        Settings.isFamily = wFamily.checked
                        Settings.isHidden = wHidden.checked
                        goBack.trigger()
                    }
                }
            }
        }
    }

    function updateModel() {
        model.isPublic = !wPrivate.checked
        model.isFriends = wFriends.checked
        model.isFamily = wFamily.checked
        model.isHidden = wHidden.checked
    }
}
