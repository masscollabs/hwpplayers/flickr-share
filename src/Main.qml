/*
 * Copyright (C) 2015 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import Flickr 1.0
import QtQuick 2.0
import Ubuntu.Components 1.3
import Ubuntu.Content 1.3

MainView {
    id: root
    applicationName: "it.mardy.uploader"
    automaticOrientation: true

    width: units.gu(40)
    height: units.gu(75)

    property bool initialized: false
    property var activeTransfer: null

    Timer {
        interval: 500; running: true; repeat: false
        onTriggered: root.initialized = true
    }

    Connections {
        target: root.activeTransfer
        onStateChanged: {
            console.log("StateChanged: " + root.activeTransfer.state)
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }
    }

    Connections {
        target: ContentHub

        onImportRequested: {
            root.activeTransfer = transfer
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }

        onShareRequested: {
            root.activeTransfer = transfer
            if (root.activeTransfer.state === ContentTransfer.Charged) {
                root.shareItems(root.activeTransfer.items)
            }
        }
    }

    Accounts {
        id: accounts

        onAuthenticatedChanged: if (authenticated) {
            uploadModel.login(consumerKey, consumerSecret, token, tokenSecret)
        }
    }

    UploadModel {
        id: uploadModel
        isPublic: Settings.isPublic
        isFriends: Settings.isFriends
        isFamily: Settings.isFamily
        isHidden: Settings.isHidden
    }

    PageStack {
        id: pageStack

        Component.onCompleted: pageStack.push(mainPage)

        Page {
            id: mainPage
            visible: false

            header: PageHeader {
                title: i18n.tr("Share to Flickr")
                flickable: imageView.visible ? imageView : null
                trailingActionBar.actions: [
                    Action {
                        iconName: "settings"
                        visible: imageView.visible
                        onTriggered: pageStack.push(Qt.resolvedUrl("SettingsPage.qml"), {
                            "model": uploadModel,
                        })
                    },
                    Action {
                        iconName: "contact"
                        onTriggered: accounts.choose()
                    }
                ]
            }

            ActivityIndicator {
                anchors.centerIn: parent
                visible: !initialized
                running: !initialized
            }

            ImageView {
                id: imageView
                anchors.fill: parent
                model: uploadModel
                accounts: accounts
                visible: initialized && uploadModel.count > 0
            }

            ImportPrompt {
                id: importPrompt
                visible: initialized && uploadModel.count === 0
                organizerLink: uploadModel.organizerLink
                onImportRequested: pageStack.push(Qt.resolvedUrl("ImportPage.qml"))
            }
        }
    }

    function shareItems(items) {
        for (var i = 0; i < items.length; i++) {
            var url = items[i].url
            console.log("Adding URL: " + url)
            uploadModel.addPhoto(url)
        }
    }
}

